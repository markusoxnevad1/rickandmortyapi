import React from 'react';

const LocationCard = (props) => {
    let {location : {name, type, dimension}} = props;
    return (
        <div className="card LocationCard">
            <div className="card-body">
                Name: {name}<br/>
                Type: {type}<br/>
                Dimension: {dimension}<br/>
            </div>
        </div>
    )
}
export default LocationCard;