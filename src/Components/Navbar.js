import React, { Component } from 'react';
import { Nav } from 'react-bootstrap';

class NavBar extends Component {
    render() {
        return (
            <>
                <Nav className="navbar navbar-dark bg-dark mb-3">
                    <div className="navbar-brand">
                        <Nav className="mr-auto">
                            <Nav.Link href={'/'}>Characters</Nav.Link>
                            <Nav.Link href={'/locations'}>Locations</Nav.Link>
                        </Nav>
                    </div>
                </Nav>
            </>
        );
    }
}
export default NavBar;