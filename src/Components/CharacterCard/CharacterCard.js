import React from 'react';
//import { stat } from 'fs';
import { Link } from 'react-router-dom';

const CharacterCard = (props) => {
    let {character : {id, name, species, status, location, origin, image}} = props;
    return (
        <div className="card CharacterCard">
            <img src={image} alt={name} />
            <div className="card-body">
                Name: {name}<br/>
                Species: {species}<br/>
                Status: {status}<br/>
                Last seen: {location.name}<br/>
                Place of origin: {origin.name}<br/>
            </div>
            <Link to={{ pathname: '/character/' + id, }} className="nav-link">View</Link>
        </div>
    )
}
export default CharacterCard;