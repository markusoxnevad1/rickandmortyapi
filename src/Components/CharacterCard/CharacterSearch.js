import React, { Component } from 'react';

class CharacterSearch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
        }
        this.handleCharacterSearch = this.handleCharacterSearch.bind(this)
        this.searchCharacter = this.searchCharacter.bind(this)
    }

    handleCharacterSearch = (e) => {
        const searchInput = e.target.value
        this.setState({ search: searchInput})
        console.log(searchInput);
    }

    searchCharacter = () => {
        if (this.props.onClick) {
            this.props.onClick(this.state.search)
        }
    }

    render () {
        return (
            <div className="search">
                <input 
                    type="text" 
                    ref={this.searchInput}
                    onChange={this.handleCharacterSearch}
                    className="form-control"
                    placeholder="Search for character"
                />
                <button onClick={this.searchCharacter}>Search</button>
            </div>
        )
    }
}
export default CharacterSearch;