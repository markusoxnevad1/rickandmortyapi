import React from 'react';
import CharacterCard from '../../../Components/CharacterCard/CharacterCard';


class CardPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            character: {},
        };
        console.log(this.props.match.params.id);
    }

    getCharacterData = () => {
        fetch(`https://rickandmortyapi.com/api/character/`+ this.props.match.params.id)
            .then(response => response.json())
            .then(data => {
                this.setState({ character: data });
            })
    };

    componentDidMount() {
        this.getCharacterData();
    }

    render() {
        return (
            <>
                {this.state.character.id ? <CharacterCard 
                    key={this.state.character.id} 
                    character={this.state.character} /> : <div>No CHARACTER</div>}
            </>
        )
    }

}
export default CardPage;