import React from 'react';
import CharacterCard from '../../../Components/CharacterCard/CharacterCard';
import CharacterSearch from '../../../Components/CharacterCard/CharacterSearch';

class CardsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rickMorty: [],
            charactererCards: [],
        };
    }

    componentDidMount(){
        this.getData("");
    }

    getData = (query) => {
        fetch('https://rickandmortyapi.com/api/character/?name='+query)
            .then(response => response.json())
            .then(data => {
                this.setState({ rickMorty: data.results });
                console.log(this.state.rickMorty);
            })
            .catch(error => console.error("something went wrong", error));
    };
    render() {
        return (
            <>
            <CharacterSearch onClick={(input)=>{this.getData(input)}} />
            <div style={{display: 'grid', gridTemplateColumns: 'auto auto auto'}}>
                {this.state.rickMorty.map(character => (
                    <CharacterCard key={character.id} showLink={true} character={character}/>
                ))
                }
            </div>
            </>
        )
    }
}
export default CardsPage;