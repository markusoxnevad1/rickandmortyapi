import React from 'react';
import LocationCard from '../../Components/LocationCard/LocationCard';

class LocationsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
        };
    }

    componentDidMount(){
        this.getData();
    }

    getData = () => {
        fetch('https://rickandmortyapi.com/api/location')
            .then(response => response.json())
            .then(data => {
                this.setState({ locations: data.results });
                console.log(this.state.locations);
            })
            .catch(error => console.error("something went wrong", error));
    };
    render() {
        return (
            <>
            <div style={{display: 'grid', gridTemplateColumns: 'auto auto auto'}}>
                {this.state.locations.map(location => (
                    <LocationCard key={location.id} showLink={true} location={location}/>
                ))
                }
            </div>
            </>
        )
    }
}
export default LocationsPage;