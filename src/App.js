import React from 'react';
//import logo from './logo.svg';
import './App.css';
import CardsPage from './Containers/CharacterCards/CardsPage/CardsPage.js';
import CardPage from './Containers/CharacterCards/CardPage/CardPage.js';
import LocationPage from './Containers/LocationCards/LocationPage';
import { Route, Switch } from 'react-router-dom';
import NavBar from './Components/Navbar';

function App() {
  return (
    <div>
      <img className="header-image" 
          src='https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720'
          alt="Rick&Morty"/>
      <NavBar />
      <Switch>
        <Route exact path="/" component={CardsPage} />
        <Route path="/character/:id" component={CardPage} />
        <Route path="/locations" component={LocationPage} />
      </Switch>
    </div>
  );
}

export default App;
